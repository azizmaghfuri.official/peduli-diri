<?php
session_start();
if (isset($_SESSION['nama'])) {
	header("Location: home.php");
}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Daftar</title>
	<style type="text/css">
		form {
			max-width: 300px;
			padding: 30px;
			font-size: 12px;
			margin: auto;
		}
		input {
			display: block;
			margin-top: 20px;
		}
		a {
			text-align: center;
		}
	</style>
</head>
<body>
	<form method="POST" action="cek_login.php">
		<input type="text" name="NIK" placeholder="NIK">
		<input type="text" name="nama" placeholder="nama">
		<input type="submit" name="submit" value="Login">
		<br/>
		<br/>
		<a href="register.php">Saya Pengguna Baru</a>
	</form>

</body>
</html>