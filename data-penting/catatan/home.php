<?php
session_start();
if (!isset($_SESSION['nama'])) {
	header("Location: login.php");
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>HOME</title>
</head>
<body>
	<div class="container">
		<div class="header">
			<h1>PEDULI DIRI</h1>
			<p>Catatan Perjalanan</p>
			<p>
				<a href="home.php">HOME</a> |
				<a href="catatan.php">Catatan Perjalanan</a> |
				<a href="isi.php">Isi Data |</a>
				<a href="logout.php">Logout</a>
			</p>
		</div>
		<div class="body">
			<p>SELAMAT DATANG USER <?=$_SESSION['nama']?></p>
			<a href="isi.php">Isi Catatan Perjalanan</a>
		</div>
		<div class="footer">
			
		</div>
	</div>
</body>
</html>